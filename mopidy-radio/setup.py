from __future__ import unicode_literals

import re

from setuptools import find_packages, setup


def get_version(filename):
    content = open(filename).read()
    metadata = dict(re.findall("__([a-z]+)__ = '([^']+)'", content))
    return metadata['version']


setup(
    name='Mopidy-Radio',
    version=get_version('mopidy_radio/__init__.py'),
    license='MIT',
    author='Lukas Lamper',
    description='Mopidy extension for playing my faviourite internet radios',
    long_description=open('README.rst').read(),
    packages=find_packages(exclude=['tests', 'tests.*']),
    zip_safe=False,
    include_package_data=True,
    python_requires='>= 2.7, < 3',
    install_requires=[
        'setuptools',
        'Mopidy >= 0.18',
        'Pykka >= 1.1'
    ],
    entry_points={
        'mopidy.ext': [
            'radio = mopidy_radio:Extension',
        ],
    },
    classifiers=[
        'Environment :: No Input/Output (Daemon)',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'Topic :: Multimedia :: Sound/Audio :: Players',
    ],
)
