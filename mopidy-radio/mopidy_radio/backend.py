import pykka
from mopidy import backend

from .library import RadioLibraryProvider


class RadioBackend(pykka.ThreadingActor, backend.Backend):
    uri_schemes = ['radio']

    def __init__(self, config, audio):
        super(RadioBackend, self).__init__()
        self.library = RadioLibraryProvider(backend=self, config=config)
        self.playlists = None
