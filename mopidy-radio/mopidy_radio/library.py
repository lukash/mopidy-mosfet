import logging
import re
from mopidy import backend, exceptions, models

logger = logging.getLogger(__name__)


class RadioLibraryProvider(backend.LibraryProvider):
    @property
    def root_directory(self):
        uri = 'radio:'
        return models.Ref.directory(name='Radios', uri=uri)

    def __init__(self, backend, config):
        super(RadioLibraryProvider, self).__init__(backend)
        self.config = config
        self.stations = self._load_stations()

    def browse(self, uri):
        result = []
        for station in self.stations.keys():
            result.append(models.Ref.track(name=self.stations[station]['name'], uri='radio://%s' % station))

        return result

    def lookup(self, uri):
        logger.info('Looking up file URI: %s', uri)
        station_id = uri.replace('radio:', '').replace('//', '')

        track = models.Track(
            name=self.stations[station_id]['name'],
            uri=self.stations[station_id]['uri']
            )
        return [track]

    def _load_stations(self):
        result = {}

        config_stations = list(self.config['radio']['stations'])
        # Stations are stored in format [Station Name](http://url)
        regexp = re.compile(r'\[([^\]]+)\]\(([^\)]+)\).*', re.M | re.DOTALL)
        for station in config_stations:
            match = regexp.match(station)
            name = match.group(1).strip()
            uri = match.group(2).strip()
            station_id = str_to_id(name)
            result[station_id] = {
                'name': name,
                'uri': uri
            }
        return result


def str_to_id(s):
    return re.sub('[^0-9a-zA-Z]+', '-', s.lower())
