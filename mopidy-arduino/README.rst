Installation
============

Install by running:

   python setup.py install 

Configuration
=============
``~/.config/mopidy/mopidy.conf``:

    [arduino]
    enabled = true
    arduino_tty = /dev/ttyUSB0
    baudrate = 115200

