from __future__ import unicode_literals

import logging
import time

import pykka
import serial
from mopidy.core import CoreListener

logger = logging.getLogger(__name__)


class ArduinoFrontend(pykka.ThreadingActor, CoreListener):

    def __init__(self, config, core):
        super(ArduinoFrontend, self).__init__()
        self.config = config
        self.core = core
        self.last_start_time = None
        self.serial = None

    def on_start(self):
        logger.info('Arduino started')
        self.serial = serial.Serial(
            port=self.config['arduino']['arduino_tty'],
            baudrate=self.config['arduino']['baudrate'],
        )
        self.__send_command('LINE1Ready')

    def track_playback_started(self, tl_track):
        track = tl_track.track

        artists = ', '.join(sorted([a.name for a in track.artists]))
        artists = unicode(artists.encode('utf-8'), errors='ignore')  # remove non-ASCII chars
        if not artists:
            artists = '-unknown-'
        track_name = unicode(track.name.encode('utf-8'), errors='ignore')  # remove non-ASCII chars
        if not track_name:
            track_name = '-unknown-'

        duration = track.length / 1000
        self.last_start_time = int(time.time())

        logger.debug('Now playing track: %s - %s', artists, track.name)
        self.__send_command('PLAY')
        self.__send_command('LINE1' + artists)
        self.__send_command('LINE2' + track_name)
        self.__send_command('TIME' + str(duration))

    def track_playback_paused(self, tl_track, time_position):
        self.__send_command('PAUSE')

    def track_playback_resumed(self, tl_track, time_position):
        self.__send_command('PLAY')

    def track_playback_ended(self, tl_track, time_position):
        track = tl_track.track
        duration = track.length and track.length // 1000 or 0
        time_position = time_position // 1000
        if self.last_start_time is None:
            self.last_start_time = int(time.time()) - duration
        self.__send_command('STOP')

    def __send_command(self, command):
        command = command + '\n'
        for c in command:
            self.serial.write(c.encode("ascii"))
