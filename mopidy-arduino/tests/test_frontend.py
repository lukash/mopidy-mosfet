import unittest

import mock

from mopidy_arduino import frontend as frontend_lib


class FrontendTest(unittest.TestCase):

    def setUp(self):
        self.config = {
            'arduino': {
                'enabled': 'true',
                'arduino_tty': '/dev/ttyUSB0',
                'baudrate': '19200'
            }
        }
        self.frontend = frontend_lib.ArduinoFrontend(
            self.config, mock.sentinel.core)

    def test_on_start_creates_lastfm_network(self):
        self.frontend.on_start()
