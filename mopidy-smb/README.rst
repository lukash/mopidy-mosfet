Installation
============
Install to Mopidy:

   python setup.py install

Configuration
=============
``~/.config/mopidy/mopidy.conf``:

    [smb]
    enabled = true
    servers = [Backup Music](smb://192.168.1.1/Backup/Music)

