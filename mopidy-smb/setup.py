from __future__ import unicode_literals

import re

from setuptools import find_packages, setup


def get_version(filename):
    content = open(filename).read()
    metadata = dict(re.findall("__([a-z]+)__ = '([^']+)'", content))
    return metadata['version']


setup(
    name='Mopidy-Smb',
    version=get_version('mopidy_smb/__init__.py'),
    license='MIT',
    author='Lukas Lamper',
    description='Mopidy extension for accessing the music over the SMB protocol',
    long_description=open('README.rst').read(),
    packages=find_packages(exclude=['tests', 'tests.*']),
    zip_safe=False,
    include_package_data=True,
    python_requires='>= 2.7, < 3',
    install_requires=[
        'setuptools',
        'Mopidy >= 0.18',
        'Pykka >= 1.1',
        'pysmbc >= 1.0.18',
        'filetype >= 1.0.5',
        'mutagen >= 1.42.0'
    ],
    entry_points={
        'mopidy.ext': [
            'smb = mopidy_smb:Extension',
        ],
    },
    classifiers=[
        'Environment :: No Input/Output (Daemon)',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2',
        'Topic :: Multimedia :: Sound/Audio :: Players',
    ],
)
