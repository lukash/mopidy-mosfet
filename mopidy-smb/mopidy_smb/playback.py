from __future__ import unicode_literals

import logging
import smbc

from mopidy import backend, audio, models
import audiotype_resolver

logger = logging.getLogger(__name__)


class SmbPlaybackProvider(backend.PlaybackProvider):

    def __init__(self, audio, backend):
        super(SmbPlaybackProvider, self).__init__(audio, backend)
        self.smbcontext = smbc.Context()
        self.playback_file = None

    def prepare_change(self):
        self.audio.emit_data(None).get()
        super(SmbPlaybackProvider, self).prepare_change()

    def change_track(self, track):
        if track.uri is None:
            return False

        self._close_previous_playback_file()
        self.playback_file = self.smbcontext.open(track.uri)

        audiotype = audiotype_resolver.audiotype_for_audiofile(self.playback_file)
        if audiotype.gst_caps() is None:
            return

        self.audio.prepare_change()
        future = self.audio.set_appsrc(
            audiotype.gst_caps(),
            need_data=self.need_data_callback,
            enough_data=self.enough_data_callback,
            seek_data=self.seek_data_callback)
        # Gapless playback requires that we block until URI change in
        # mopidy.audio has completed before we return from change_track().
        future.get()

        self.audio.set_metadata(models.Track(
            uri=track.uri,
            name="Enhanced name",
            artists=[models.Artist(name="Amazing name")],
            album=models.Album(name="Just an album")
        ))



        return True

    def need_data_callback(self, length_hint):
        # This callback is called from GStreamer/the GObject event loop.
        logger.debug(
            'Audio requested more data (hint=%s); accepting deliveries',
            length_hint)

        #duration = self.audio.calculate_duration(length_hint, '44100')
        buffer_ = audio.utils.create_buffer(
            self.playback_file.read(512000))

        # We must block here to know if the buffer was consumed successfully.
        consumed = self.audio.emit_data(buffer_)
        # No .get() blocking needed? It hangs on track change sometimes, when buffer is not consumed

        if consumed:
            logger.info("CONSUMMEDDDD")
        else:
            logger.info("NOT CONSUMMEDDDD")

        return 2

    def enough_data_callback(self):
        # This callback is called from GStreamer/the GObject event loop.
        logger.debug('Audio has enough data; rejecting deliveries')

    def seek_data_callback(self, time_position):
        # This callback is called from GStreamer/the GObject event loop.
        # It forwards the call to the backend actor.
        logger.info('Seeking >>>>>>>>>>>>> %s' % time_position)
        return True

    def _close_previous_playback_file(self):
        if self.playback_file is not None:
            #self.audio.emit_data(None)
            self.playback_file.close()
            self.playback_file = None
