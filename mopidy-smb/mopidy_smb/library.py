import logging
import os
import re
import urllib

import smbc
from mopidy import backend, models


logger = logging.getLogger(__name__)


class SmbLibraryProvider(backend.LibraryProvider):
    @property
    def root_directory(self):
        uri = 'smb://'
        return models.Ref.directory(name='SMB Network', uri=uri)

    def __init__(self, backend, config):
        super(SmbLibraryProvider, self).__init__(backend)
        self.config = config
        self.servers = self._load_servers()
        self.smbcontext = smbc.Context()

    def browse(self, uri):
        logger.info('Browsing SMB URI: %s', uri)
        result = []
        if uri == 'smb://':
            logger.info('Browsing SMB root URI')
            for server in self.servers.keys():
                logger.info('server %s', server)
                result.append(models.Ref.track(name=self.servers[server]['name'], uri=self.servers[server]['uri']))
        else:
            entries = self.smbcontext.opendir(uri).getdents()
            for entry in entries:
                print(entry)
                if entry.smbc_type == smbc.DIR:
                    result.append(models.Ref.directory(name=entry.name, uri=uri + '/' + entry.name))
                elif entry.smbc_type == smbc.FILE:
                    result.append(models.Ref.track(name=entry.name, uri=uri + '/' + entry.name))

        return result

    def lookup(self, uri):
        logger.info('Looking up file URI: %s', uri)
        filename = _filename_from_smb_uri(uri)
        return [models.Track(
            uri=uri,
            name=filename
        )]

    def _load_servers(self):
        result = {}
        config_servers = list(self.config['smb']['servers'])
        # Servers are stored in format [Station Name](http://url)
        regexp = re.compile(r'\[([^\]]+)\]\(([^\)]+)\).*', re.M | re.DOTALL)
        for server in config_servers:
            match = regexp.match(server)
            name = match.group(1).strip()
            uri = match.group(2).strip()
            server_id = str_to_id(name)
            result[server_id] = {
                'name': name,
                'uri': uri
            }
        return result


def _filename_from_smb_uri(smb_uri):
    if smb_uri is None:
        return None
    uri_decoded = urllib.unquote(smb_uri).decode('utf8')
    return os.path.basename(uri_decoded)


def str_to_id(s):
    return re.sub('[^0-9a-zA-Z]+', '-', s.lower())
