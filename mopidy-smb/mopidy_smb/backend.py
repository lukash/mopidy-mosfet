import pykka
from mopidy import backend

from .library import SmbLibraryProvider
from .playback import SmbPlaybackProvider


class SmbBackend(pykka.ThreadingActor, backend.Backend):
    uri_schemes = ['smb']

    def __init__(self, config, audio):
        super(SmbBackend, self).__init__()
        self.library = SmbLibraryProvider(backend=self, config=config)
        self.playback = SmbPlaybackProvider(audio=audio, backend=self)
        self.playlists = None
