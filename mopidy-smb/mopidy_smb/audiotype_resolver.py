import logging
import filetype
import urllib
import os
from mopidy import models
from mutagen.flac import FLAC
from mutagen.mp3 import MP3

logger = logging.getLogger(__name__)


def audiotype_for_smb_uri(uri, smbcontext):
    '''
    @type smbcontext: smbc.Context
    @rtype: AudioType
    '''

    audiofile = smbcontext.open(uri)
    audiotype = audiotype_for_audiofile(audiofile, uri, smbcontext)
    audiofile.close()

    return audiotype


def audiotype_for_audiofile(audiofile, uri=None, smbcontext=None):
    buf = bytearray(audiofile.read(4))
    audiofile.seek(0)
    kind = filetype.guess(buf)
    if kind is None:
        logger.warn('Cannot guess file type!')
        return None

    for t in audiotypes:
        if t.mime() == kind.mime:
            return t(uri, smbcontext)

    logger.warn('Unsupported file type - %s' % kind.mime)
    return None


class AudioType:

    def __init__(self, uri=None, smbcontext=None):
        self.uri = uri
        self.smbcontext = smbcontext

    @staticmethod
    def mime():
        pass

    @staticmethod
    def gst_caps():
        pass

    def track_details(self):
        pass


class AudioTypeMP3(AudioType):
    @staticmethod
    def mime():
        return 'audio/mpeg'

    @staticmethod
    def gst_caps():
        return 'audio/mpeg,mpegversion=1,layer=3'

    def track_details(self):
        if self.smbcontext is None:
            logger.error("No SMB context provided")
            filename = _filename_from_smb_uri(self.uri)
            return models.Track(
                uri=self.uri,
                name=filename
            )

        audiofile = self.smbcontext.open(self.uri)
        tags = MP3(SmbFileWrapFile(audiofile))
        audiofile.close()

        return models.Track(
            uri=self.uri,
            name=str(tags['TIT2']),
            length=long(tags.info.length),
            bitrate=long(tags.info.bitrate),
            artists=[models.Artist(name=str(tags['TPE1']))],
            album=models.Album(name=str(tags['TALB']))
        )


class AudioTypeFLAC(AudioType):
    @staticmethod
    def mime():
        return 'audio/x-flac'

    @staticmethod
    def gst_caps():
        return 'audio/x-flac'

    def track_details(self):
        #if self.smbcontext is None:
        logger.error("No SMB context provided")
        filename = _filename_from_smb_uri(self.uri)
        return models.Track(
            uri=self.uri,
            name=filename
        )

        #audiofile = self.smbcontext.open(self.uri)
        #tags = FLAC(SmbFileWrapFile(audiofile))
        #audiofile.close()


class AudioTypeOGG(AudioType):

    @staticmethod
    def mime():
        return 'audio/ogg'

    @staticmethod
    def gst_caps():
        return 'audio/ogg'


def _filename_from_smb_uri(smb_uri):
    if smb_uri is None:
        return None
    uri_decoded = urllib.unquote(smb_uri).decode('utf8')
    return os.path.basename(uri_decoded)


class SmbFileWrapFile(file):
    def __init__(self, smbc_file):
        '''
        @type smbc_file: smbc.File
        '''
        self.position = 0
        self.file = smbc_file

    def seek(self, offset, whence=0):
        self.position = self.file.seek(offset, whence)
        return self.position

    def read(self, size=None):
        if size is None:
            read_bytes = self.file.read()
            self.position += len(read_bytes)
        else:
            read_bytes = self.file.read(size)
            self.position += size
        return read_bytes

    def tell(self):
        return self.position

    def close(self):
        #self.file.close()
        pass


audiotypes = [AudioTypeMP3, AudioTypeFLAC, AudioTypeOGG]